package persistance.bean;

/**
 * UtilisateurDO Class
 * 
 * @author Valentin J.
 */
public class UtilisateurDO {
	private Long    id;
	private String  nom;
	private String  prenom;
	private Integer age;

	/**
	 * Constructor
	 */
	public UtilisateurDO() {
		super();
	}

	/**
	 * Init UtilisateurDO
	 *
	 * @param  idDO     Id to set
	 * @param  nomDO    Nom to set
	 * @param  prenomDO Prenom to set
	 * @param  ageDO    Age to set
	 * @return          UtilisateurDO
	 */
	public UtilisateurDO initUtilisateurDO(final Long idDO, final String nomDO, final String prenomDO, final Integer ageDO) {
		this.id = idDO;
		this.nom = nomDO;
		this.prenom = prenomDO;
		this.age = ageDO;
		return this;
	}

	/**
	 * Get the value of id
	 *
	 * @return the id
	 */
	public Long getId() {
		return this.id;
	}

	/**
	 * Set new value to id
	 *
	 * @param id the id to set
	 */
	public void setId(final Long id) {
		this.id = id;
	}

	/**
	 * Get the value of nom
	 *
	 * @return the nom
	 */
	public String getNom() {
		return this.nom;
	}

	/**
	 * Set new value to nom
	 *
	 * @param nom the nom to set
	 */
	public void setNom(final String nom) {
		this.nom = nom;
	}

	/**
	 * Get the value of prenom
	 *
	 * @return the prenom
	 */
	public String getPrenom() {
		return this.prenom;
	}

	/**
	 * Set new value to prenom
	 *
	 * @param prenom the prenom to set
	 */
	public void setPrenom(final String prenom) {
		this.prenom = prenom;
	}

	/**
	 * Get the value of age
	 *
	 * @return the age
	 */
	public Integer getAge() {
		return this.age;
	}

	/**
	 * Set new value to age
	 *
	 * @param age the age to set
	 */
	public void setAge(final Integer age) {
		this.age = age;
	}

}
