package persistance.dao.impl;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import persistance.bean.UtilisateurDO;
import persistance.dao.IUtilisateurDAO;
import persistance.factory.HibernateFactory;

/**
 * UtilisateurDAO Class
 * 
 * @author Valentin J.
 */
public class UtilisateurDAO implements IUtilisateurDAO {
	private final SessionFactory  sessionFactory = HibernateFactory.getSessionFactory();

	private static UtilisateurDAO instance       = null;

	/**
	 * Method to get instance of Class
	 * 
	 * @return Instance of class
	 */
	public static UtilisateurDAO getInstance() {
		return instance == null ? new UtilisateurDAO() : instance;
	}

	@Override
	public List<UtilisateurDO> findAll() {
		try (final Session session = this.sessionFactory.openSession()) {
			final Transaction transaction = session.beginTransaction();
			final Query<UtilisateurDO> query = session.createQuery("From UtilisateurDO", UtilisateurDO.class);

			final List<UtilisateurDO> utilisateurDO = query.getResultList();

			session.flush();
			transaction.commit();

			return utilisateurDO;
		} catch (final HibernateException e) {
			e.printStackTrace();
		}

		return Collections.emptyList();
	}

	@Override
	public UtilisateurDO findById(final Long id) {
		System.out.println("Méthode findById du DAO");

		try (final Session session = this.sessionFactory.openSession()) {
			final Transaction transaction = session.beginTransaction();
			final Query<UtilisateurDO> query = session.createQuery("From UtilisateurDO where id = :idUtilisateurDO", UtilisateurDO.class);

			query.setParameter("idUtilisateurDO", id);

			final Optional<UtilisateurDO> userDO = query.uniqueResultOptional();

			session.flush();
			transaction.commit();

			return userDO.orElse(null);
		} catch (final HibernateException e) {
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public UtilisateurDO create(final UtilisateurDO user) {
		System.out.println("Méthode create du DAO");

		try (final Session session = this.sessionFactory.openSession()) {
			final Transaction transaction = session.beginTransaction();

			session.save(user);
			session.flush();

			transaction.commit();

			return user;
		} catch (final HibernateException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public UtilisateurDO update(final UtilisateurDO user) {
		try (final Session session = this.sessionFactory.openSession()) {
			final Transaction transaction = session.beginTransaction();
			final Query<?> query = session
					.createQuery("update UtilisateurDO set nom = :nom, prenom = :prenom, age = :age where id = :idUtilisateurDO");

			query.setParameter("nom", user.getNom());
			query.setParameter("prenom", user.getPrenom());
			query.setParameter("age", user.getAge());
			query.setParameter("idUtilisateurDO", user.getId());

			query.executeUpdate();

			session.flush();
			transaction.commit();

			return user;
		} catch (final HibernateException e) {
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public boolean delete(final Long id) {
		try (final Session session = this.sessionFactory.openSession()) {
			final Transaction transaction = session.beginTransaction();
			final Query<?> query = session.createQuery("delete from UtilisateurDO where id = :idUtilisateurDO");

			query.setParameter("idUtilisateurDO", id);

			final int result = query.executeUpdate();

			session.flush();
			transaction.commit();

			return result == 1;
		} catch (final HibernateException e) {
			e.printStackTrace();
		}

		return false;
	}
}
