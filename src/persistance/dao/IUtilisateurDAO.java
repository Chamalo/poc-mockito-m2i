package persistance.dao;

import java.util.List;

import persistance.bean.UtilisateurDO;

/**
 * IUtilisateurDAO Interface
 * 
 * @author Valentin J.
 */
public interface IUtilisateurDAO {

	/**
	 * Method to find everything inside a table
	 * 
	 * @return List of UtilisateurDO, or empty collection
	 */
	List<UtilisateurDO> findAll();

	/**
	 * Method to find Utilisateur
	 * 
	 * @param  id ID of Utilisateur
	 * @return    UtilisateurDO if found, null otherwise
	 */
	UtilisateurDO findById(final Long id);

	/**
	 * Method to save into Database
	 * 
	 * @param  user Utilisateur to create
	 * @return      UtilisateurDO created with id generated
	 */
	UtilisateurDO create(final UtilisateurDO user);

	/**
	 * Method to update an Utilisateur into Database
	 * 
	 * @param  user UtilisateurDO to update
	 * @return      UtilisateurDO updated
	 */
	UtilisateurDO update(final UtilisateurDO user);

	/**
	 * Method to delete into Database
	 * 
	 * @param  id id of Utilisateur to delete
	 * @return    boolean true if success, false otherwise
	 */
	boolean delete(final Long id);
}
