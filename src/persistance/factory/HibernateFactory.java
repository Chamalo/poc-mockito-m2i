package persistance.factory;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * HibernateFactory class
 * 
 * @author Valentin J.
 */
public class HibernateFactory {
	private static SessionFactory sessionFactory = null;

	private HibernateFactory() {
	}

	/**
	 * Method to create sessionFactory singleton
	 * 
	 * @return sessionFactory
	 */
	public static SessionFactory getSessionFactory() {
		synchronized (HibernateFactory.class) {
			if (sessionFactory == null) {
				sessionFactory = new Configuration().configure().buildSessionFactory();
			}
			return sessionFactory;
		}
	}
}