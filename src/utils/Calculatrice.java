package utils;

/**
 * Calculatrice Class
 * 
 * @author Valentin J.
 */
public class Calculatrice {

	private CalculUtil calcul;

	/**
	 * Constructor
	 */
	public Calculatrice() {
		super();
	}

	/**
	 * Method to make an addition
	 * 
	 * @param  a Number
	 * @param  b Number
	 * @return   Sum of a & b
	 */
	public int addition(final int a, final int b) {
		return this.calcul.somme(a, b);
	}

	/**
	 * Get the value of calcul
	 *
	 * @return the calcul
	 */
	public CalculUtil getCalcul() {
		return this.calcul;
	}

	/**
	 * Set new value to calcul
	 *
	 * @param calcul the calcul to set
	 */
	public void setCalcul(final CalculUtil calcul) {
		this.calcul = calcul;
	}

}
