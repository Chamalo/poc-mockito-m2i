package utils;

/**
 * CalculUtil class <br>
 * Use to make calculs
 * 
 * @author Valentin J.
 */
public class CalculUtil {

	/**
	 * Method to make sum of 2 numbers
	 * 
	 * @param  a Number
	 * @param  b Number
	 * @return   Sum of a & b
	 */
	public int somme(final int a, final int b) {
		return a + b;
	}
}
