package presentation.bean;

import java.io.Serializable;

/**
 * Classe representant un Utilisateur
 * 
 * @author  xsintive
 * @version 1.0
 */
public class UtilisateurDto implements Serializable {

	private static final long serialVersionUID = 3654297383714722490L;
	private Long              id;
	private String            nom;
	private String            prenom;
	private int               age;

	/**
	 * Constructeur par defaut
	 */
	public UtilisateurDto() {
		// empty method
	}

	/**
	 * Permet d'initialiser un utilisateur
	 * 
	 * @param  idUser     l'id de l'utilisateur
	 * @param  nomUser    le nom de l'utilisateur
	 * @param  prenomUser le prenom de l'utilisateur
	 * @param  ageUser    l'age de l'utilisateur
	 * @return            l'utilisateur initialis�
	 */
	public UtilisateurDto initUtilisateur(final Long idUser, final String nomUser, final String prenomUser, final int ageUser) {
		this.id = idUser;
		this.nom = nomUser;
		this.prenom = prenomUser;
		this.age = ageUser;
		return this;
	}

	/**
	 * Getter for id
	 *
	 * @return the id
	 */
	public Long getId() {
		return this.id;
	}

	/**
	 * Setter for id
	 *
	 * @param id the id to set
	 */
	public void setId(final Long id) {
		this.id = id;
	}

	/**
	 * Getter for nom
	 *
	 * @return the nom
	 */
	public String getNom() {
		return this.nom;
	}

	/**
	 * Setter for nom
	 *
	 * @param nom the nom to set
	 */
	public void setNom(final String nom) {
		this.nom = nom;
	}

	/**
	 * Getter for prenom
	 *
	 * @return the prenom
	 */
	public String getPrenom() {
		return this.prenom;
	}

	/**
	 * Setter for prenom
	 *
	 * @param prenom the prenom to set
	 */
	public void setPrenom(final String prenom) {
		this.prenom = prenom;
	}

	/**
	 * Get the value of age
	 *
	 * @return the age
	 */
	public int getAge() {
		return this.age;
	}

	/**
	 * Set new value to age
	 *
	 * @param age the age to set
	 */
	public void setAge(final int age) {
		this.age = age;
	}

}
