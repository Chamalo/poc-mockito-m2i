package metier;

import java.util.List;

import presentation.bean.UtilisateurDto;

/**
 * IUtilisateurService Interface
 * 
 * @author Valentin J.
 */
public interface IUtilisateurService {

	/**
	 * Method to get one Utilisateur
	 * 
	 * @param  id Id of Utilisateur to found
	 * @return    Utilisateur or null if not found
	 */
	UtilisateurDto recupUtilisateur(final Long id);

	/**
	 * Method to create an Utilisateur
	 * 
	 * @param  userDto Utilisateur to create
	 * @return         Created UtilisateurDto
	 */
	UtilisateurDto creerUtilisateur(final UtilisateurDto userDto);

	/**
	 * Method to update an Utilisateur
	 * 
	 * @param  userDto Utilisateur to update
	 * @return         Updated UtilisateurDto
	 */
	UtilisateurDto updateUtilisateur(final UtilisateurDto userDto);

	/**
	 * Method to get all users
	 * 
	 * @return List of UtilisateurDto
	 */
	List<UtilisateurDto> recupererToutUtilisateurs();

	/**
	 * Method to delete an Utilisateur
	 * 
	 * @param  id Id of Utilisateur to delete
	 * @return    boolean, true if deleted, false otherwise
	 */
	boolean supprimerUtilisateur(final Long id);
}
