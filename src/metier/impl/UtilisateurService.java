package metier.impl;

import java.util.List;

import metier.IUtilisateurService;
import metier.UtilisateurMapper;
import persistance.bean.UtilisateurDO;
import persistance.dao.IUtilisateurDAO;
import persistance.dao.impl.UtilisateurDAO;
import presentation.bean.UtilisateurDto;

/**
 * UtilisateurService class, implements IUtilisateurService
 * 
 * @author Valentin J.
 */
public class UtilisateurService implements IUtilisateurService {

	private IUtilisateurDAO           utilisateurDAO = UtilisateurDAO.getInstance();
	private static UtilisateurService instance       = null;

	@Override
	public UtilisateurDto recupUtilisateur(final Long id) {
		final UtilisateurDO userDO = this.utilisateurDAO.findById(id);

		return (userDO != null ? UtilisateurMapper.mapperToDto(userDO) : null);
	}

	@Override
	public UtilisateurDto creerUtilisateur(final UtilisateurDto userDto) {
		final UtilisateurDO userDO = this.utilisateurDAO.findById(userDto.getId());

		if (userDO == null) {
			return UtilisateurMapper.mapperToDto(this.utilisateurDAO.create(UtilisateurMapper.mapperToDo(userDto)));
		}

		return null;
	}

	@Override
	public boolean supprimerUtilisateur(final Long id) {
		return this.utilisateurDAO.delete(id);
	}

	@Override
	public UtilisateurDto updateUtilisateur(final UtilisateurDto userDto) {
		return UtilisateurMapper.mapperToDto(this.utilisateurDAO.update(UtilisateurMapper.mapperToDo(userDto)));
	}

	@Override
	public List<UtilisateurDto> recupererToutUtilisateurs() {
		final List<UtilisateurDO> listUserDO = this.utilisateurDAO.findAll();

		return UtilisateurMapper.mapperToListDto(listUserDO);
	}

	/**
	 * Method to get instance of Class
	 * 
	 * @return Instance of class
	 */
	public UtilisateurService getInstance() {
		return instance == null ? new UtilisateurService() : instance;
	}

	/**
	 * Get the value of utilisateurDAO
	 *
	 * @return the utilisateurDAO
	 */
	public IUtilisateurDAO getUtilisateurDAO() {
		return this.utilisateurDAO;
	}

	/**
	 * Set new value to utilisateurDAO
	 *
	 * @param utilisateurDAO the utilisateurDAO to set
	 */
	public void setUtilisateurDAO(final IUtilisateurDAO utilisateurDAO) {
		this.utilisateurDAO = utilisateurDAO;
	}
}
