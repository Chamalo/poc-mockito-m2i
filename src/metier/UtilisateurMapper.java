package metier;

import java.util.List;
import java.util.stream.Collectors;

import persistance.bean.UtilisateurDO;
import presentation.bean.UtilisateurDto;

/**
 * Class for mapping Utilisateur
 * 
 * @author Valentin J.
 */
public class UtilisateurMapper {

	/**
	 * Constructor
	 */
	private UtilisateurMapper() {
	}

	/**
	 * Method to map UtilisateurDO into UtilisateurDTO
	 * 
	 * @param  utilisateurDo Utilisateur to map
	 * @return               Mapped UtilisateurDto
	 */
	public static UtilisateurDto mapperToDto(final UtilisateurDO utilisateurDo) {
		return new UtilisateurDto().initUtilisateur(utilisateurDo.getId(), utilisateurDo.getNom(), utilisateurDo.getPrenom(),
				utilisateurDo.getAge());
	}

	/**
	 * Method to map UtilisateurDto into UtilisateurDO
	 * 
	 * @param  utilisateurDto Utilisateur to map
	 * @return                Mapped UtilisateurDO
	 */
	public static UtilisateurDO mapperToDo(final UtilisateurDto utilisateurDto) {
		return new UtilisateurDO().initUtilisateurDO(utilisateurDto.getId(), utilisateurDto.getNom(), utilisateurDto.getPrenom(),
				utilisateurDto.getAge());
	}

	/**
	 * Method to map List of UtilisateurDO into List of UtilisateurDTO
	 * 
	 * @param  utilisateurDoList Utilisateur List to map
	 * @return                   Mapped List UtilisateurDto
	 */
	public static List<UtilisateurDto> mapperToListDto(final List<UtilisateurDO> utilisateurDoList) {
		// UtilisateurMapper::mapperToDto = user -> mapperToDto(user)
		return (utilisateurDoList.stream().map(UtilisateurMapper::mapperToDto).collect(Collectors.toList()));
	}
}
