package metier.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

import org.hibernate.HibernateException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import metier.IUtilisateurService;
import persistance.bean.UtilisateurDO;
import persistance.dao.IUtilisateurDAO;
import presentation.bean.UtilisateurDto;

/**
 * UtilisateurServiceTest test class
 * 
 * @author Valentin J.
 */
class UtilisateurServiceTest {

	// Classe que l'on souhaite tester
	@InjectMocks
	private static final IUtilisateurService service = new UtilisateurService().getInstance();

	// Classe dont le comportement va etre imiter
	@Mock
	private IUtilisateurDAO                  utilisateurDAO;

	// Initialisation des classes mockées
	@BeforeEach
	void initMock() {
		MockitoAnnotations.openMocks(this);
	}

	/**
	 * Test method for {@link metier.impl.UtilisateurService#findById(Integer)} <br>
	 * This one test not null return
	 */
	@Test
	void testFind() {
		// Sans annotation
		// final IUtilisateurDAO userDAO = mock(UtilisateurDAO.class);

		final String nom = "nom test";
		final String prenom = "prenom test";
		final Long id = 125664L;
		final Integer age = 28;

		when(this.utilisateurDAO.findById(1L)).thenReturn(new UtilisateurDO().initUtilisateurDO(id, nom, prenom, age));

		final UtilisateurDto userDto = service.recupUtilisateur(1L);

		assertNotNull(userDto);
		assertEquals(nom, userDto.getNom());
		assertEquals(prenom, userDto.getPrenom());
		assertEquals(age, userDto.getAge());
		assertEquals(id, userDto.getId());
	}

	/**
	 * Test method for {@link metier.impl.UtilisateurService#findById(Integer)} <br>
	 * This one test null return
	 */
	@Test
	void testFindNull() {
		when(this.utilisateurDAO.findById(0L)).thenThrow(new HibernateException("Impossible de trouver un utilisateur avec l'id 0"));

		try {
			final UtilisateurDto userDto = service.recupUtilisateur(0L);
		} catch (final HibernateException h) {
			h.printStackTrace();
		}
	}

	/**
	 * Test method for {@link metier.impl.UtilisateurService#creerUtilisateur(UtilisateurDto)}
	 */
	@Test
	void testCreate() {
		UtilisateurDto userDto = new UtilisateurDto().initUtilisateur(null, "Dupont", "Marc", 34);

		// Intercept all calls with param of type UtilisateurDO
		when(this.utilisateurDAO.create(Mockito.any(UtilisateurDO.class)))
				.thenReturn(new UtilisateurDO().initUtilisateurDO(1L, "Ted", "Bill", 55));

		userDto = service.creerUtilisateur(userDto);

		assertNotNull(userDto);
		assertEquals(1L, userDto.getId());
		assertEquals("Ted", userDto.getNom());
		assertEquals("Bill", userDto.getPrenom());
		assertEquals(55, userDto.getAge());
	}

	/**
	 * Test method for {@link metier.impl.UtilisateurService#creerUtilisateur(UtilisateurDto)} <br>
	 * This one test null return
	 */
	@Test
	void testCreateNull() {
		UtilisateurDto userDto = new UtilisateurDto().initUtilisateur(null, "Dupont", "Maurice", 32);

		// Intercept all calls with param of type UtilisateurDO
		when(this.utilisateurDAO.create(Mockito.any(UtilisateurDO.class)))
				.thenThrow(new HibernateException("Impossible d'ajouter l'utilisateur"));

		try {
			userDto = service.creerUtilisateur(userDto);
		} catch (final HibernateException e) {
			e.printStackTrace();
		}
	}
}
