package utils;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

/**
 * Calculatrice Test Class with Mockito
 * 
 * @author Valentin
 */
class CalculatriceTest {

	@InjectMocks
	private static final Calculatrice calculatrice = new Calculatrice();

	@Mock
	private CalculUtil                calculUtil;

	@BeforeEach
	void initMock() {
		MockitoAnnotations.openMocks(this);
	}

	/**
	 * Method to test when + thenReturn
	 */
	@Test
	void testThenReturn() {
		when(this.calculUtil.somme(1, 1)).thenReturn(11);

		assertEquals(11, calculatrice.addition(1, 1));
	}

	/**
	 * Method to test with any int
	 */
	@Test
	void testAnyInt() {
		when(this.calculUtil.somme(Mockito.anyInt(), Mockito.anyInt())).thenReturn(404);

		assertEquals(404, calculatrice.addition(1, 1));
		assertEquals(404, calculatrice.addition(1, 1));
	}

	/**
	 * Method to test thenTrow
	 */
	@Test
	void testThenThrow() {
		when(this.calculUtil.somme(2, 5)).thenThrow(new NumberFormatException("Erreur !!!"));

		try {
			calculatrice.addition(2, 5);
		} catch (final NumberFormatException e) {
			e.printStackTrace();
		}
	}
}
